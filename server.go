package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/kalaijagdai/my-project/controller"
	"gitlab.com/kalaijagdai/my-project/service"
)

var (
	videoService    service.VideoService       = service.New()
	videoController controller.VideoController = controller.New(videoService)
)

func main() {
	server := gin.Default()
	server.Use(gin.Recovery())
	server.Use(gin.Logger())
	server.GET("/videos", func(ctx *gin.Context) {
		ctx.JSON(200, videoController.FindAll())
	})
	server.POST("/videos", func(ctx *gin.Context) {
		ctx.JSON(200, videoController.Save(ctx))
	})
	server.Run(":9090")
}
